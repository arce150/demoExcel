﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExcelBussines
{
    public class Expediente
    {
        public int IdExpediente { get; set; }
        public string Descripcion { get; set; }
        public int Cantidad { get; set; }
        public DateTime Fecha { get; set; }  
    }
    public class ExpedienteBl {
        public List<Expediente> ListarExpediente() {
            List<Expediente> lista = new List<Expediente>();
            lista.Add(new Expediente() {IdExpediente=1,Descripcion="Articulo" ,Cantidad=2,Fecha=DateTime.Now});
            lista.Add(new Expediente() { IdExpediente = 1, Descripcion = "Articulo1", Cantidad = 2, Fecha = DateTime.Now });
            lista.Add(new Expediente() { IdExpediente = 2, Descripcion = "Articulo2", Cantidad = 3, Fecha = DateTime.Now.AddDays(1) });
            lista.Add(new Expediente() { IdExpediente = 3, Descripcion = "Articulo3", Cantidad = 42, Fecha = DateTime.Now });
            lista.Add(new Expediente() { IdExpediente = 4, Descripcion = "Articulo4", Cantidad = 5, Fecha = DateTime.Now });
            lista.Add(new Expediente() { IdExpediente = 5, Descripcion = "Articulo5", Cantidad = 6, Fecha = DateTime.Now });
            return lista;
        }
    }
}
