﻿using System;
using System.Collections.Generic;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using A = DocumentFormat.OpenXml.Drawing;
using Xdr = DocumentFormat.OpenXml.Drawing.Spreadsheet;
using A14 = DocumentFormat.OpenXml.Office2010.Drawing;
using System.IO;
using System.Linq;
namespace ExcelBussines
{
    public class XmlExcel
    {
        #region "Metodos"

        static List<Expediente> _listaExpediente = new List<Expediente>();
        public static byte[] GenerarExcel(List<Expediente> lista)
        {

            _listaExpediente = lista;
            var memoryStream = new MemoryStream();
            using (var excel = SpreadsheetDocument.Create(memoryStream, SpreadsheetDocumentType.Workbook, true))
            {
                CreateParts(excel);
            }

            return memoryStream.ToArray();
        }

        private static void CreateParts(SpreadsheetDocument excel)
        {
            //Generando Libro y Hojas
            WorkbookPart workbookPart1 = excel.AddWorkbookPart();
            GenerateWorkbookPart1Content(workbookPart1);

            //Generando Estilos a Nivel del Libro
            WorkbookStylesPart workbookStylesPart1 = workbookPart1.AddNewPart<WorkbookStylesPart>();
            GenerateWorkbookStylesPart1Content(workbookStylesPart1);

            //Generando contenido de hojas
            int i = 1;
            string HojaActual = String.Empty;
            string HojaID = String.Empty;
            string DrawingID = String.Empty;
            string ImagenID = String.Empty;

            HojaActual = "Gastos";
            HojaID = "hoja" + i.ToString();
            DrawingID = "Drawing" + i.ToString();
            ImagenID = "Imagen" + i.ToString();

            WorksheetPart worksheetPart1 = workbookPart1.AddNewPart<WorksheetPart>(HojaID);
            GenerateWorksheetPart1Content(worksheetPart1, HojaActual);

            DrawingsPart drawingsPart1 = worksheetPart1.AddNewPart<DrawingsPart>(DrawingID);
            GenerateDrawingsPart1Content(drawingsPart1, ImagenID);

        }

        private static void GenerateWorkbookPart1Content(WorkbookPart workbookPart1)
        {
            //Generando estructura de hojas
            Workbook workbook1 = new Workbook();
            Sheets sheets1 = new Sheets();
            int i = 1;
            sheets1.Append(new Sheet() { Name = "Expedientes", SheetId = Convert.ToUInt32(i), Id = "hoja" + i.ToString() });
            workbook1.Append(sheets1);
            workbookPart1.Workbook = workbook1;
        }

        private static void GenerateWorkbookStylesPart1Content(WorkbookStylesPart workbookStylesPart1)
        {
            Stylesheet stylesheet1 = new Stylesheet();

            //FONTS (FontId)
            Fonts fonts1 = new Fonts();
            //FontId: 0U
            Font font0 = new Font();
            fonts1.Append(font0);
            //FontId: 1U (titulo)
            Font font1 = new Font();
            font1.Append(new Bold());
            //font1.Append(new Underline());
            font1.Append(new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 12D });
            font1.Append(new Color() { Theme = (UInt32Value)1U });
            font1.Append(new FontName() { Val = "Cambria" });
            font1.Append(new FontFamilyNumbering() { Val = 2 });
            fonts1.Append(font1);
            //FontId: 2U (etiqueta)
            Font font2 = new Font();
            font2.Append(new Bold());
            font2.Append(new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 12D });
            font2.Append(new Color() { Theme = (UInt32Value)1U });
            font2.Append(new FontName() { Val = "Cambria" });
            font2.Append(new FontFamilyNumbering() { Val = 2 });
            fonts1.Append(font2);
            //FontId: 3U (valor)
            Font font3 = new Font();
            font3.Append(new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 8D });
            font3.Append(new Color() { Theme = (UInt32Value)1U });
            font3.Append(new FontName() { Val = "Arial" });
            font3.Append(new FontFamilyNumbering() { Val = 2 });
            fonts1.Append(font3);
            //FontId: 4U (valor azul)
            Font font4 = new Font();
            font4.Append(new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 8D });
            font4.Append(new Color() { Indexed = (UInt32Value)12U });
            font4.Append(new FontName() { Val = "Arial" });
            font4.Append(new FontFamilyNumbering() { Val = 2 });
            fonts1.Append(font4);
            //FontId: 5U
            Font font5 = new Font();
            font5.Append(new Bold());
            font5.Append(new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 8D });
            font5.Append(new Color() { Indexed = (UInt32Value)12U });
            font5.Append(new FontName() { Val = "Arial" });
            font5.Append(new FontFamilyNumbering() { Val = 2 });
            fonts1.Append(font5);

            //FontId: 6U
            Font font6 = new Font();
            font6.Append(new Bold());
            font6.Append(new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 9D });
            font6.Append(new Color() { Theme = (UInt32Value)1U });
            font6.Append(new FontName() { Val = "Cambria" });
            font6.Append(new FontFamilyNumbering() { Val = 2 });
            fonts1.Append(font6);

            //FontId: 7U
            Font font7 = new Font();
            font7.Append(new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 8D });
            font7.Append(new Color() { Theme = (UInt32Value)1U });
            font7.Append(new FontName() { Val = "Cambria" });
            font7.Append(new FontFamilyNumbering() { Val = 2 });
            fonts1.Append(font7);

            //FontId: 8U
            Font font8 = new Font();
            font8.Append(new DocumentFormat.OpenXml.Spreadsheet.FontSize() { Val = 9D });
            font8.Append(new Color() { Theme = (UInt32Value)1U });
            font8.Append(new FontName() { Val = "Cambria" });
            font8.Append(new FontFamilyNumbering() { Val = 2 });
            fonts1.Append(font8);


            //FILLS (FillId)
            Fills fills1 = new Fills();
            //FillId: 0U
            Fill fill0 = new Fill();
            fills1.Append(fill0);
            //FillId: 1U
            Fill fill1 = new Fill(new PatternFill(new ForegroundColor() { Rgb = "FFC00000" },
                new BackgroundColor() { Indexed = (UInt32Value)64U })
            { PatternType = PatternValues.Solid });
            fills1.Append(fill1);

            //FillId: 2U color de titulo cabeceras
            Fill fill2 = new Fill();
            PatternFill patternFill4 = new PatternFill() { PatternType = PatternValues.Solid };
            ForegroundColor foregroundColor2 = new ForegroundColor() { Rgb = "CCCCCC" };
            BackgroundColor backgroundColor2 = new BackgroundColor() { Indexed = (UInt32Value)64U };
            patternFill4.Append(foregroundColor2);
            patternFill4.Append(backgroundColor2);
            fill2.Append(patternFill4);
            fills1.Append(fill2);


            //FillId: 3U
            Fill fill3 = new Fill();
            PatternFill patternFill5 = new PatternFill() { PatternType = PatternValues.Solid };
            ForegroundColor foregroundColor3 = new ForegroundColor() { Rgb = "D9D9D9" };
            BackgroundColor backgroundColor3 = new BackgroundColor() { Indexed = (UInt32Value)64U };
            patternFill5.Append(foregroundColor3);
            patternFill5.Append(backgroundColor3);
            fill3.Append(patternFill5);
            fills1.Append(fill3);

            //FillId: 4U
            Fill fill4 = new Fill();
            PatternFill patternFill6 = new PatternFill() { PatternType = PatternValues.Solid };
            ForegroundColor foregroundColor4 = new ForegroundColor() { Rgb = "92D050" };
            BackgroundColor backgroundColor4 = new BackgroundColor() { Indexed = (UInt32Value)64U };
            patternFill6.Append(foregroundColor4);
            patternFill6.Append(backgroundColor4);
            fill4.Append(patternFill6);
            fills1.Append(fill4);


            //BORDERS (BorderId)
            Borders borders1 = new Borders();
            //BorderId: 0U
            borders1.Append(new Border());
            //BorderId: 1U
            Border border2 = new Border();
            border2.Append(new LeftBorder(new Color() { Indexed = (UInt32Value)64U }) { Style = BorderStyleValues.Thin });
            border2.Append(new RightBorder(new Color() { Indexed = (UInt32Value)64U }) { Style = BorderStyleValues.Thin });
            border2.Append(new TopBorder(new Color() { Indexed = (UInt32Value)64U }) { Style = BorderStyleValues.Thin });
            border2.Append(new BottomBorder(new Color() { Indexed = (UInt32Value)64U }) { Style = BorderStyleValues.Thin });
            border2.Append(new DiagonalBorder());
            borders1.Append(border2);
            //BorderId: 2U
            Border border3 = new Border();
            border3.Append(new LeftBorder(new Color() { Indexed = (UInt32Value)64U }) { Style = BorderStyleValues.Thin });
            border3.Append(new RightBorder(new Color() { Indexed = (UInt32Value)64U }) { Style = BorderStyleValues.Thin });
            border3.Append(new DiagonalBorder());
            borders1.Append(border3);
            //BorderId: 3U
            Border border4 = new Border();
            border4.Append(new TopBorder(new Color() { Indexed = (UInt32Value)64U }) { Style = BorderStyleValues.Thin });
            border4.Append(new DiagonalBorder());
            borders1.Append(border4);
            //BorderId: 4U
            Border border5 = new Border();
            border5.Append(new LeftBorder(new Color() { Indexed = (UInt32Value)64U }) { Style = BorderStyleValues.Medium });
            border5.Append(new RightBorder(new Color() { Indexed = (UInt32Value)64U }) { Style = BorderStyleValues.Medium });
            border5.Append(new TopBorder(new Color() { Indexed = (UInt32Value)64U }) { Style = BorderStyleValues.Thin });
            border5.Append(new BottomBorder(new Color() { Indexed = (UInt32Value)64U }) { Style = BorderStyleValues.Medium });
            border5.Append(new DiagonalBorder());
            borders1.Append(border5);
            //CELLFORMATS (StyleIndex)
            CellFormats cellFormats1 = new CellFormats();
            //StyleIndex: 0U
            CellFormat cellFormat0 = new CellFormat();
            cellFormats1.Append(cellFormat0);
            //StyleIndex: 1U - (titulo)
            CellFormat cellFormat1 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Center }) { FontId = (UInt32Value)1U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U };
            cellFormats1.Append(cellFormat1);
            //StyleIndex: 2U - (etiqueta - derecha)
            CellFormat cellFormat2 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Right }) { FontId = (UInt32Value)2U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U };
            cellFormats1.Append(cellFormat2);
            //StyleIndex: 3U - (valor - texto)
            CellFormat cellFormat3 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Left }) { FontId = (UInt32Value)3U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U };
            cellFormats1.Append(cellFormat3);
            //StyleIndex: 4U - (valor - numero)
            CellFormat cellFormat4 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Right }) { FontId = (UInt32Value)3U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, NumberFormatId = (UInt32Value)4U };
            cellFormats1.Append(cellFormat4);
            //StyleIndex: 5U - (etiqueta - izquierda)
            CellFormat cellFormat5 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Left }) { FontId = (UInt32Value)2U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U };
            cellFormats1.Append(cellFormat5);
            //StyleIndex: 6U - (valor - texto - azul - fecha)
            CellFormat cellFormat6 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Center }) { FontId = (UInt32Value)4U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, NumberFormatId = (UInt32Value)14U };
            cellFormats1.Append(cellFormat6);
            //StyleIndex: 7U - (valor - numero - azul)
            CellFormat cellFormat7 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Right }) { FontId = (UInt32Value)4U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, NumberFormatId = (UInt32Value)4U };
            cellFormats1.Append(cellFormat7);
            //StyleIndex: 8U - (valor - texto - centrado)
            CellFormat cellFormat8 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Center }) { FontId = (UInt32Value)3U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U };
            cellFormats1.Append(cellFormat8);
            //StyleIndex: 9U - (valor - texto - fecha)
            CellFormat cellFormat9 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Center }) { FontId = (UInt32Value)3U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, NumberFormatId = (UInt32Value)14U };
            cellFormats1.Append(cellFormat9);
            //StyleIndex: 10U - (valor - texto - derecha)
            CellFormat cellFormat10 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Right }) { FontId = (UInt32Value)3U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U };
            cellFormats1.Append(cellFormat10);
            //StyleIndex: 11U - (nota)
            CellFormat cellFormat11 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Left, Vertical = VerticalAlignmentValues.Top, WrapText = true }) { FontId = (UInt32Value)2U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U };
            cellFormats1.Append(cellFormat11);
            //StyleIndex: 12U - (firma)
            CellFormat cellFormat12 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Center }) { FontId = (UInt32Value)2U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U };
            cellFormats1.Append(cellFormat12);
            //StyleIndex: 13U - (valor - azul - derecha - 7)(texto compensatorio)
            CellFormat cellFormat13 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Left }) { FontId = (UInt32Value)8U, BorderId = (UInt32Value)1U };
            cellFormats1.Append(cellFormat13);
            //StyleIndex: 14U - (valor - azul - izquierda - 7)(texto moratorio)
            CellFormat cellFormat14 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Left }) { FontId = (UInt32Value)8U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)0U, NumberFormatId = (UInt32Value)14U };
            cellFormats1.Append(cellFormat14);
            //StyleIndex: 15U - (titulo iteraciones)
            CellFormat cellFormat15 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Center, Vertical = VerticalAlignmentValues.Center, WrapText = true }) { FontId = (UInt32Value)6U, FillId = (UInt32Value)2U, BorderId = (UInt32Value)1U };
            cellFormats1.Append(cellFormat15);
            //StyleIndex: 16U - (subtotales)
            CellFormat cellFormat16 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Right }) { FontId = (UInt32Value)7U, FillId = (UInt32Value)2U, BorderId = (UInt32Value)1U, NumberFormatId = (UInt32Value)4U };
            cellFormats1.Append(cellFormat16);
            //StyleIndex: 17U - (valores numericos dentro de tabla)
            CellFormat cellFormat17 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Right }) { FontId = (UInt32Value)3U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)1U, NumberFormatId = (UInt32Value)4U };
            cellFormats1.Append(cellFormat17);
            //StyleIndex: 18U - (valores lineas dentro de tabla)
            CellFormat cellFormat18 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Right }) { FontId = (UInt32Value)2U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)2U };
            cellFormats1.Append(cellFormat18);
            //StyleIndex: 19U - (valores lineas subtotales de tabla)
            CellFormat cellFormat19 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Right }) { FontId = (UInt32Value)2U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)1U };
            cellFormats1.Append(cellFormat19);
            //StyleIndex: 20U - (valor - texto - fecha - lineas)
            CellFormat cellFormat20 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Center }) { FontId = (UInt32Value)3U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)1U, NumberFormatId = (UInt32Value)14U };
            cellFormats1.Append(cellFormat20);
            //StyleIndex: 21U - (valores texto centrado dentro de tabla)
            CellFormat cellFormat21 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Center }) { FontId = (UInt32Value)3U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)2U };
            cellFormats1.Append(cellFormat21);
            //StyleIndex: 22U - (valores texto izquierda dentro de tabla)
            CellFormat cellFormat22 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Left }) { FontId = (UInt32Value)3U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)2U };
            cellFormats1.Append(cellFormat22);
            //StyleIndex: 23U - (valores texto izquierda dentro de tabla)
            CellFormat cellFormat23 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Right }) { FontId = (UInt32Value)8U, FillId = (UInt32Value)2U, BorderId = (UInt32Value)4U, NumberFormatId = (UInt32Value)4U };
            cellFormats1.Append(cellFormat23);


            CellFormat cellFormat24 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Right }) { FontId = (UInt32Value)6U, FillId = (UInt32Value)0U, BorderId = (UInt32Value)1U, NumberFormatId = (UInt32Value)4U };
            cellFormats1.Append(cellFormat24);


            //StyleIndex: 25U - (titulo iteraciones)
            CellFormat cellFormat25 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Right }) { FontId = (UInt32Value)6U, FillId = (UInt32Value)3U, BorderId = (UInt32Value)1U, NumberFormatId = (UInt32Value)4U };
            cellFormats1.Append(cellFormat25);

            //StyleIndex: 26U - (titulo iteraciones)
            CellFormat cellFormat26 = new CellFormat(new Alignment() { Horizontal = HorizontalAlignmentValues.Right }) { FontId = (UInt32Value)6U, FillId = (UInt32Value)4U, BorderId = (UInt32Value)1U, NumberFormatId = (UInt32Value)4U };
            cellFormats1.Append(cellFormat26);


            stylesheet1.Append(fonts1);
            stylesheet1.Append(fills1);
            stylesheet1.Append(borders1);
            stylesheet1.Append(cellFormats1);

            workbookStylesPart1.Stylesheet = stylesheet1;
        }

        private static void GenerateWorksheetPart1Content(WorksheetPart worksheetPart1, string textoHojaActual)
        {
            Worksheet worksheet1 = new Worksheet();

            //Definicion de columnas
            Columns columns1 = new Columns();
            columns1.Append(new Column() { Min = (UInt32Value)1U, Max = (UInt32Value)1U, Width = 10D, BestFit = true, CustomWidth = true });//A
            columns1.Append(new Column() { Min = (UInt32Value)2U, Max = (UInt32Value)2U, Width = 10D, BestFit = true, CustomWidth = true }); //B
            columns1.Append(new Column() { Min = (UInt32Value)3U, Max = (UInt32Value)3U, Width = 30D, BestFit = true, CustomWidth = true }); //C
            columns1.Append(new Column() { Min = (UInt32Value)4U, Max = (UInt32Value)4U, Width = 40D, BestFit = true, CustomWidth = true }); //D
            columns1.Append(new Column() { Min = (UInt32Value)5U, Max = (UInt32Value)5U, Width = 15D, BestFit = true, CustomWidth = true }); //E
            columns1.Append(new Column() { Min = (UInt32Value)6U, Max = (UInt32Value)6U, Width = 40D, BestFit = true, CustomWidth = true });//F
            columns1.Append(new Column() { Min = (UInt32Value)7U, Max = (UInt32Value)7U, Width = 7D, BestFit = true, CustomWidth = true });//G
            columns1.Append(new Column() { Min = (UInt32Value)8U, Max = (UInt32Value)8U, Width = 14D, BestFit = true, CustomWidth = true });//H
            columns1.Append(new Column() { Min = (UInt32Value)9U, Max = (UInt32Value)9U, Width = 21D, BestFit = true, CustomWidth = true });//I
            columns1.Append(new Column() { Min = (UInt32Value)10U, Max = (UInt32Value)10U, Width = 7D, BestFit = true, CustomWidth = true });//J
            columns1.Append(new Column() { Min = (UInt32Value)11U, Max = (UInt32Value)11U, Width = 14D, BestFit = true, CustomWidth = true });//K
            columns1.Append(new Column() { Min = (UInt32Value)12U, Max = (UInt32Value)12U, Width = 8D, BestFit = true, CustomWidth = true });//L
            columns1.Append(new Column() { Min = (UInt32Value)13U, Max = (UInt32Value)13U, Width = 17D, BestFit = true, CustomWidth = true });//M
            columns1.Append(new Column() { Min = (UInt32Value)14U, Max = (UInt32Value)14U, Width = 11D, BestFit = true, CustomWidth = true });//N
            columns1.Append(new Column() { Min = (UInt32Value)15U, Max = (UInt32Value)15U, Width = 10D, BestFit = true, CustomWidth = true });//O
            columns1.Append(new Column() { Min = (UInt32Value)16U, Max = (UInt32Value)16U, Width = 10D, BestFit = true, CustomWidth = true });//P
            columns1.Append(new Column() { Min = (UInt32Value)17U, Max = (UInt32Value)17U, Width = 9D, BestFit = true, CustomWidth = true });//Q
            columns1.Append(new Column() { Min = (UInt32Value)18U, Max = (UInt32Value)18U, Width = 10D, BestFit = true, CustomWidth = true });//R
            columns1.Append(new Column() { Min = (UInt32Value)19U, Max = (UInt32Value)19U, Width = 7D, BestFit = true, CustomWidth = true });//S
            columns1.Append(new Column() { Min = (UInt32Value)20U, Max = (UInt32Value)20U, Width = 9D, BestFit = true, CustomWidth = true });//T
            columns1.Append(new Column() { Min = (UInt32Value)21U, Max = (UInt32Value)21U, Width = 12D, BestFit = true, CustomWidth = true });//U 
            columns1.Append(new Column() { Min = (UInt32Value)22U, Max = (UInt32Value)22U, Width = 7D, BestFit = true, CustomWidth = true });//V
            columns1.Append(new Column() { Min = (UInt32Value)23U, Max = (UInt32Value)23U, Width = 11D, BestFit = true, CustomWidth = true });//W
            columns1.Append(new Column() { Min = (UInt32Value)24U, Max = (UInt32Value)24U, Width = 11D, BestFit = true, CustomWidth = true });//X
            columns1.Append(new Column() { Min = (UInt32Value)25U, Max = (UInt32Value)25U, Width = 12D, BestFit = true, CustomWidth = true });//Y
            columns1.Append(new Column() { Min = (UInt32Value)26U, Max = (UInt32Value)26U, Width = 12D, BestFit = true, CustomWidth = true });//Z
            columns1.Append(new Column() { Min = (UInt32Value)27U, Max = (UInt32Value)27U, Width = 12D, BestFit = true, CustomWidth = true });//AA
            columns1.Append(new Column() { Min = (UInt32Value)28U, Max = (UInt32Value)28U, Width = 8D, BestFit = true, CustomWidth = true });//AB
            columns1.Append(new Column() { Min = (UInt32Value)29U, Max = (UInt32Value)29U, Width = 8D, BestFit = true, CustomWidth = true });//AC
            columns1.Append(new Column() { Min = (UInt32Value)30U, Max = (UInt32Value)30U, Width = 8D, BestFit = true, CustomWidth = true });//AD
            columns1.Append(new Column() { Min = (UInt32Value)31U, Max = (UInt32Value)31U, Width = 8D, BestFit = true, CustomWidth = true });//AE
            columns1.Append(new Column() { Min = (UInt32Value)32U, Max = (UInt32Value)32U, Width = 11D, BestFit = true, CustomWidth = true });//AF
            columns1.Append(new Column() { Min = (UInt32Value)33U, Max = (UInt32Value)33U, Width = 11D, BestFit = true, CustomWidth = true });//AG
            columns1.Append(new Column() { Min = (UInt32Value)34U, Max = (UInt32Value)34U, Width = 11D, BestFit = true, CustomWidth = true });//AH
            columns1.Append(new Column() { Min = (UInt32Value)35U, Max = (UInt32Value)35U, Width = 11D, BestFit = true, CustomWidth = true });//AI
            columns1.Append(new Column() { Min = (UInt32Value)36U, Max = (UInt32Value)36U, Width = 11D, BestFit = true, CustomWidth = true });//AJ
            columns1.Append(new Column() { Min = (UInt32Value)37U, Max = (UInt32Value)37U, Width = 11D, BestFit = true, CustomWidth = true });//AK
            columns1.Append(new Column() { Min = (UInt32Value)38U, Max = (UInt32Value)38U, Width = 11D, BestFit = true, CustomWidth = true });//AL
            columns1.Append(new Column() { Min = (UInt32Value)39U, Max = (UInt32Value)39U, Width = 11D, BestFit = true, CustomWidth = true });//AM
            columns1.Append(new Column() { Min = (UInt32Value)40U, Max = (UInt32Value)40U, Width = 11D, BestFit = true, CustomWidth = true });//AN
            columns1.Append(new Column() { Min = (UInt32Value)41U, Max = (UInt32Value)41U, Width = 11D, BestFit = true, CustomWidth = true });//AO
            columns1.Append(new Column() { Min = (UInt32Value)42U, Max = (UInt32Value)42U, Width = 11D, BestFit = true, CustomWidth = true });//AP
            columns1.Append(new Column() { Min = (UInt32Value)43U, Max = (UInt32Value)43U, Width = 11D, BestFit = true, CustomWidth = true });//AQ
            columns1.Append(new Column() { Min = (UInt32Value)44U, Max = (UInt32Value)44U, Width = 11D, BestFit = true, CustomWidth = true });//AR
            columns1.Append(new Column() { Min = (UInt32Value)45U, Max = (UInt32Value)45U, Width = 11D, BestFit = true, CustomWidth = true });//AS
            columns1.Append(new Column() { Min = (UInt32Value)46U, Max = (UInt32Value)46U, Width = 11D, BestFit = true, CustomWidth = true });//AT
            columns1.Append(new Column() { Min = (UInt32Value)47U, Max = (UInt32Value)47U, Width = 11D, BestFit = true, CustomWidth = true });//AU
            columns1.Append(new Column() { Min = (UInt32Value)48U, Max = (UInt32Value)48U, Width = 11D, BestFit = true, CustomWidth = true });//AV
            columns1.Append(new Column() { Min = (UInt32Value)49U, Max = (UInt32Value)49U, Width = 11D, BestFit = true, CustomWidth = true });//AW
            columns1.Append(new Column() { Min = (UInt32Value)50U, Max = (UInt32Value)50U, Width = 11D, BestFit = true, CustomWidth = true });//AX
            columns1.Append(new Column() { Min = (UInt32Value)51U, Max = (UInt32Value)51U, Width = 11D, BestFit = true, CustomWidth = true });//AY
            columns1.Append(new Column() { Min = (UInt32Value)52U, Max = (UInt32Value)52U, Width = 11D, BestFit = true, CustomWidth = true });//AZ
            columns1.Append(new Column() { Min = (UInt32Value)53U, Max = (UInt32Value)53U, Width = 11D, BestFit = true, CustomWidth = true });//BA
            columns1.Append(new Column() { Min = (UInt32Value)54U, Max = (UInt32Value)54U, Width = 11D, BestFit = true, CustomWidth = true });//BB
            columns1.Append(new Column() { Min = (UInt32Value)55U, Max = (UInt32Value)55U, Width = 11D, BestFit = true, CustomWidth = true });//BC
            columns1.Append(new Column() { Min = (UInt32Value)56U, Max = (UInt32Value)56U, Width = 11D, BestFit = true, CustomWidth = true });//BD
            columns1.Append(new Column() { Min = (UInt32Value)57U, Max = (UInt32Value)57U, Width = 11D, BestFit = true, CustomWidth = true });//BE
            columns1.Append(new Column() { Min = (UInt32Value)58U, Max = (UInt32Value)58U, Width = 11D, BestFit = true, CustomWidth = true });//BF
            columns1.Append(new Column() { Min = (UInt32Value)59U, Max = (UInt32Value)59U, Width = 11D, BestFit = true, CustomWidth = true });//BG
            columns1.Append(new Column() { Min = (UInt32Value)60U, Max = (UInt32Value)60U, Width = 11D, BestFit = true, CustomWidth = true });//BH
            columns1.Append(new Column() { Min = (UInt32Value)61U, Max = (UInt32Value)61U, Width = 11D, BestFit = true, CustomWidth = true });//BI
            columns1.Append(new Column() { Min = (UInt32Value)62U, Max = (UInt32Value)62U, Width = 11D, BestFit = true, CustomWidth = true });//BJ
            columns1.Append(new Column() { Min = (UInt32Value)63U, Max = (UInt32Value)63U, Width = 11D, BestFit = true, CustomWidth = true });//BK
            columns1.Append(new Column() { Min = (UInt32Value)64U, Max = (UInt32Value)64U, Width = 11D, BestFit = true, CustomWidth = true });//BL
            columns1.Append(new Column() { Min = (UInt32Value)65U, Max = (UInt32Value)65U, Width = 11D, BestFit = true, CustomWidth = true });//BM
            columns1.Append(new Column() { Min = (UInt32Value)66U, Max = (UInt32Value)66U, Width = 11D, BestFit = true, CustomWidth = true });//BN
            columns1.Append(new Column() { Min = (UInt32Value)67U, Max = (UInt32Value)67U, Width = 11D, BestFit = true, CustomWidth = true });//BO
            columns1.Append(new Column() { Min = (UInt32Value)68U, Max = (UInt32Value)68U, Width = 11D, BestFit = true, CustomWidth = true });//BP
            columns1.Append(new Column() { Min = (UInt32Value)69U, Max = (UInt32Value)69U, Width = 11D, BestFit = true, CustomWidth = true });//BQ
            columns1.Append(new Column() { Min = (UInt32Value)70U, Max = (UInt32Value)70U, Width = 11D, BestFit = true, CustomWidth = true });//BR
            columns1.Append(new Column() { Min = (UInt32Value)71U, Max = (UInt32Value)71U, Width = 11D, BestFit = true, CustomWidth = true });//BS
            columns1.Append(new Column() { Min = (UInt32Value)72U, Max = (UInt32Value)72U, Width = 11D, BestFit = true, CustomWidth = true });//BT
            columns1.Append(new Column() { Min = (UInt32Value)73U, Max = (UInt32Value)73U, Width = 11D, BestFit = true, CustomWidth = true });//BU
            columns1.Append(new Column() { Min = (UInt32Value)74U, Max = (UInt32Value)74U, Width = 11D, BestFit = true, CustomWidth = true });//BV
            columns1.Append(new Column() { Min = (UInt32Value)75U, Max = (UInt32Value)75U, Width = 11D, BestFit = true, CustomWidth = true });//BW
            columns1.Append(new Column() { Min = (UInt32Value)76U, Max = (UInt32Value)76U, Width = 11D, BestFit = true, CustomWidth = true });//BX
            columns1.Append(new Column() { Min = (UInt32Value)77U, Max = (UInt32Value)77U, Width = 11D, BestFit = true, CustomWidth = true });//BY
            columns1.Append(new Column() { Min = (UInt32Value)78U, Max = (UInt32Value)78U, Width = 11D, BestFit = true, CustomWidth = true });//BZ
            //Configuracion de pagin
            SheetFormatProperties sheetFormatProperties1 = new SheetFormatProperties()
            {
                DefaultRowHeight = 12D,
                DyDescent = 0.25D
            };
            PageMargins pageMargins1 = new PageMargins()
            {
                Left = 0.43307086614173229D,
                Right = 0.43307086614173229D,
                Top = 0.74803149606299213D,
                Bottom = 0.74803149606299213D,
                Header = 0.31496062992125984D,
                Footer = 0.31496062992125984D
            }; //(1.1 derecha e 1.1 izquierda)
            PageSetup pageSetup1 = new PageSetup()
            {
                PaperSize = (UInt32Value)9U,
                Orientation = OrientationValues.Portrait
            };

            //Data de filas
            SheetData sheetData1 = new SheetData();

            //Contenedor de Merge de columnas
            MergeCells mergeCells1 = new MergeCells(); 

            //Fila 3  
            Row row3 = new Row() { RowIndex = (UInt32Value)3U, Height = 15.75D, DyDescent = 0.25D, };
            row3.Append(new Cell() { CellReference = "B3", StyleIndex = 1U, DataType = CellValues.String, CellValue = new CellValue("REPORTE DE OBSERVACIONES") });
            sheetData1.Append(row3);
            mergeCells1.Append(new MergeCell() { Reference = "B3:F3" });

            //Fila 7 : cabecera tabla (I)
            int rowNumber = 7;
            Row row6 = new Row() { RowIndex = UInt32.Parse(rowNumber.ToString()), DyDescent = 0.2D, Height = 16D, CustomHeight = true };
            row6.Append(new Cell() { CellReference = "A" + rowNumber.ToString(), StyleIndex = 15U, DataType = CellValues.String, CellValue = new CellValue("ÍTEM") });
            row6.Append(new Cell() { CellReference = "B" + rowNumber.ToString(), StyleIndex = 15U, DataType = CellValues.String, CellValue = new CellValue("VIÁTICO") });
            row6.Append(new Cell() { CellReference = "C" + rowNumber.ToString(), StyleIndex = 15U, DataType = CellValues.String, CellValue = new CellValue("ASUNTO") });
            row6.Append(new Cell() { CellReference = "D" + rowNumber.ToString(), StyleIndex = 15U, DataType = CellValues.String, CellValue = new CellValue("OBSERVACIONES") });
            row6.Append(new Cell() { CellReference = "E" + rowNumber.ToString(), StyleIndex = 15U, DataType = CellValues.String, CellValue = new CellValue("FECHA") });
            row6.Append(new Cell() { CellReference = "F" + rowNumber.ToString(), StyleIndex = 15U, DataType = CellValues.String, CellValue = new CellValue("OBSERVADOR") });
            sheetData1.Append(row6);

            //Fila  contenido  
            Row row=null; 
            foreach (var item in _listaExpediente)
            {
                rowNumber = rowNumber + 1; 
                row = new Row() { RowIndex = UInt32.Parse(rowNumber.ToString()), DyDescent = 0.2D, Height = 17D, CustomHeight = true };
                row.Append(new Cell() { CellReference = "A" + rowNumber.ToString(), StyleIndex = 13U, DataType = CellValues.Number, CellValue = new CellValue(item.IdExpediente.ToString()) });
                row.Append(new Cell() { CellReference = "B" + rowNumber.ToString(), StyleIndex = 13U, DataType = CellValues.String, CellValue = new CellValue(item.Descripcion) });
                //row.Append(new Cell() { CellReference = "C" + rowNumber.ToString(), StyleIndex = 13U, DataType = CellValues.String, CellValue = new CellValue(item.AsuntoViatico) });
                //row.Append(new Cell() { CellReference = "D" + rowNumber.ToString(), StyleIndex = 17U, DataType = CellValues.String, CellValue = new CellValue(item.Observacion) });
                //row.Append(new Cell() { CellReference = "E" + rowNumber.ToString(), StyleIndex = 13U, DataType = CellValues.String, CellValue = new CellValue(item.FechaRegistro.ToString("dd/MM/yyyy")) });
                //row.Append(new Cell() { CellReference = "F" + rowNumber.ToString(), StyleIndex = 17U, DataType = CellValues.String, CellValue = new CellValue(string.Concat(item.NombreRegistrador, " ", item.ApellidosRegistrador)) });
                sheetData1.Append(row);

            } 
            worksheet1.Append(sheetFormatProperties1);
            worksheet1.Append(columns1);
            worksheet1.Append(sheetData1);
            worksheet1.Append(mergeCells1);
            worksheet1.Append(pageMargins1);
            worksheet1.Append(pageSetup1); 
            worksheetPart1.Worksheet = worksheet1;
        }
        private static void GenerateDrawingsPart1Content(DrawingsPart drawingsPart1, string idImagen)
        {
            Xdr.WorksheetDrawing worksheetDrawing1 = new Xdr.WorksheetDrawing();
            worksheetDrawing1.AddNamespaceDeclaration("xdr", "http://schemas.openxmlformats.org/drawingml/2006/spreadsheetDrawing");
            worksheetDrawing1.AddNamespaceDeclaration("a", "http://schemas.openxmlformats.org/drawingml/2006/main");

            Xdr.TwoCellAnchor twoCellAnchor1 = new Xdr.TwoCellAnchor() { EditAs = Xdr.EditAsValues.OneCell };

            Xdr.FromMarker fromMarker1 = new Xdr.FromMarker();
            Xdr.ColumnId columnId1 = new Xdr.ColumnId();
            columnId1.Text = "1";
            Xdr.ColumnOffset columnOffset1 = new Xdr.ColumnOffset();
            columnOffset1.Text = "0";
            Xdr.RowId rowId1 = new Xdr.RowId();
            Xdr.RowOffset rowOffset1 = new Xdr.RowOffset();
            rowOffset1.Text = "0";

            fromMarker1.Append(columnId1);
            fromMarker1.Append(columnOffset1);
            fromMarker1.Append(rowId1);
            fromMarker1.Append(rowOffset1);

            Xdr.ToMarker toMarker1 = new Xdr.ToMarker();
            Xdr.ColumnId columnId2 = new Xdr.ColumnId();
            columnId2.Text = "4"; //Columnas que ocupara la imagen
            Xdr.ColumnOffset columnOffset2 = new Xdr.ColumnOffset();
            columnOffset2.Text = "-900000"; //quitará ancho a imagen (eg: -100000)
            Xdr.RowId rowId2 = new Xdr.RowId();
            //rowId2.Text = (3 + CantidadFilasDefecto + _cantidadFilas).ToString(); //Filas que ocupara la imagen
            Xdr.RowOffset rowOffset2 = new Xdr.RowOffset();
            rowOffset2.Text = "0"; //quitará alto a imagen (eg: -100000)

            toMarker1.Append(columnId2);
            toMarker1.Append(columnOffset2);
            toMarker1.Append(rowId2);
            toMarker1.Append(rowOffset2);

            Xdr.Picture picture1 = new Xdr.Picture();

            Xdr.NonVisualPictureProperties nonVisualPictureProperties1 = new Xdr.NonVisualPictureProperties();
            Xdr.NonVisualDrawingProperties nonVisualDrawingProperties1 = new Xdr.NonVisualDrawingProperties() { Id = (UInt32Value)2U, Name = "Picture 1" };

            Xdr.NonVisualPictureDrawingProperties nonVisualPictureDrawingProperties1 = new Xdr.NonVisualPictureDrawingProperties();
            A.PictureLocks pictureLocks1 = new A.PictureLocks() { NoChangeAspect = true, NoChangeArrowheads = true };

            nonVisualPictureDrawingProperties1.Append(pictureLocks1);

            nonVisualPictureProperties1.Append(nonVisualDrawingProperties1);
            nonVisualPictureProperties1.Append(nonVisualPictureDrawingProperties1);

            Xdr.BlipFill blipFill1 = new Xdr.BlipFill();

            A.Blip blip1 = new A.Blip() { Embed = idImagen };
            blip1.AddNamespaceDeclaration("r", "http://schemas.openxmlformats.org/officeDocument/2006/relationships");

            A.BlipExtensionList blipExtensionList1 = new A.BlipExtensionList();

            A.BlipExtension blipExtension1 = new A.BlipExtension() { Uri = "{28A0092B-C50C-407E-A947-70E740481C1C}" };

            A14.UseLocalDpi useLocalDpi1 = new A14.UseLocalDpi() { Val = false };
            useLocalDpi1.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");

            blipExtension1.Append(useLocalDpi1);

            blipExtensionList1.Append(blipExtension1);

            blip1.Append(blipExtensionList1);
            A.SourceRectangle sourceRectangle1 = new A.SourceRectangle();

            A.Stretch stretch1 = new A.Stretch();
            A.FillRectangle fillRectangle1 = new A.FillRectangle();

            stretch1.Append(fillRectangle1);

            blipFill1.Append(blip1);
            blipFill1.Append(sourceRectangle1);
            blipFill1.Append(stretch1);

            Xdr.ShapeProperties shapeProperties1 = new Xdr.ShapeProperties() { BlackWhiteMode = A.BlackWhiteModeValues.Auto };

            A.Transform2D transform2D1 = new A.Transform2D();
            A.Offset offset1 = new A.Offset() { X = 0L, Y = 0L };
            A.Extents extents1 = new A.Extents() { Cx = 0L, Cy = 0L };

            transform2D1.Append(offset1);
            transform2D1.Append(extents1);

            A.PresetGeometry presetGeometry1 = new A.PresetGeometry() { Preset = A.ShapeTypeValues.Rectangle };
            A.AdjustValueList adjustValueList1 = new A.AdjustValueList();

            presetGeometry1.Append(adjustValueList1);
            A.NoFill noFill1 = new A.NoFill();

            A.ShapePropertiesExtensionList shapePropertiesExtensionList1 = new A.ShapePropertiesExtensionList();

            A.ShapePropertiesExtension shapePropertiesExtension1 = new A.ShapePropertiesExtension() { Uri = "{909E8E84-426E-40DD-AFC4-6F175D3DCCD1}" };

            A14.HiddenFillProperties hiddenFillProperties1 = new A14.HiddenFillProperties();
            hiddenFillProperties1.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main");

            A.SolidFill solidFill6 = new A.SolidFill();
            A.RgbColorModelHex rgbColorModelHex14 = new A.RgbColorModelHex() { Val = "FFFFFF" };

            solidFill6.Append(rgbColorModelHex14);

            hiddenFillProperties1.Append(solidFill6);

            shapePropertiesExtension1.Append(hiddenFillProperties1);

            shapePropertiesExtensionList1.Append(shapePropertiesExtension1);

            shapeProperties1.Append(transform2D1);
            shapeProperties1.Append(presetGeometry1);
            shapeProperties1.Append(noFill1);
            shapeProperties1.Append(shapePropertiesExtensionList1);

            picture1.Append(nonVisualPictureProperties1);
            picture1.Append(blipFill1);
            picture1.Append(shapeProperties1);
            Xdr.ClientData clientData1 = new Xdr.ClientData();

            twoCellAnchor1.Append(fromMarker1);
            twoCellAnchor1.Append(toMarker1);
            twoCellAnchor1.Append(picture1);
            twoCellAnchor1.Append(clientData1);

            worksheetDrawing1.Append(twoCellAnchor1);

            drawingsPart1.WorksheetDrawing = worksheetDrawing1;
        }


        #endregion
    }
}
