﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
namespace DemoWeb.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public string GenerarPdfv2()
        {
            String pdfPath = System.Web.Hosting.HostingEnvironment.MapPath("~/App_Data/pdf.pdf");
            byte[] pdfBytes = System.IO.File.ReadAllBytes(pdfPath);
            string pdfBase64 = Convert.ToBase64String(pdfBytes);
            return string.Concat("data:application/pdf;base64,", pdfBase64);
        }
        public FileResult GenerarPdf()
        {
            string fileName = "pruebapdf";
            string path = AppDomain.CurrentDomain.BaseDirectory + "App_Data/pdf.pdf";
            return File(path + fileName, System.Net.Mime.MediaTypeNames.Application.Pdf, fileName);
        }
        public string ExportarExcel()
        {
            List<ExcelBussines.Expediente> listaExpedientes = new ExcelBussines.ExpedienteBl().ListarExpediente();
            string nombreArchivo = string.Concat("Documentos_", DateTime.Now.Ticks, ".xlsx");
            byte[] byteArchivos = ExcelBussines.XmlExcel.GenerarExcel(listaExpedientes);
            //String rutaArchivos = System.Web.Hosting.HostingEnvironment.MapPath("~/Descargas/");
            //System.IO.File.WriteAllBytes(string.Concat(rutaArchivos, nombreArchivo), byteArchivos);
            string pdfBase64 = Convert.ToBase64String(byteArchivos);
            return string.Concat("data:application/pdf;base64,", pdfBase64); ;
            //return nombreArchivo;
        }
        public string EnviarSms(string numeroDestino,string mensaje) {
            string filasAfectadas = string.Empty;
            SendSms( numeroDestino,   mensaje).Wait();
            return filasAfectadas;
        }
        static async Task SendSms( string numeroDestino, string mensaje) {
            string accountSid = System.Configuration.ConfigurationManager.AppSettings.Get("accountSid");
            string authToken = System.Configuration.ConfigurationManager.AppSettings.Get("authToken");
            string numeroOrigen= System.Configuration.ConfigurationManager.AppSettings.Get("from");
            try
            {
                TwilioClient.Init(accountSid, authToken);
                var message = await MessageResource.CreateAsync(
                    body: mensaje,
                    from: new Twilio.Types.PhoneNumber(numeroOrigen),
                    to: new Twilio.Types.PhoneNumber(string.Concat("+51" ,numeroDestino))
                );
            }
            catch (Exception ex) {
                throw ex;
            }
            
        }
    }
}